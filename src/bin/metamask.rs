use std::time::SystemTime;
use jsonrpsee::core::client::ClientT;
use jsonrpsee::rpc_params;
use jsonrpsee::ws_client::WsClientBuilder;
use rand::random;

/*async fn mk_client(url: &String) -> Result<Arc<Client>, Error> {
    match WsClientBuilder::default().build(url).await {
        Ok(c) => Ok(Arc::new(c)),
        Err(e) => Err(e)
    }
}

async fn loop_(tx_size: i32, c: Arc<Client>) {
    loop {
        let random_bytes: Vec<u8> = (0..tx_size).map(|_| { random::<u8>() }).collect();
        let response: String = c.request("submit_transaction", rpc_params![random_bytes]).await.unwrap();
        tracing::info!("response: {}", response);
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let addr = std::env::args().nth(1).expect("no addr given");

    tracing::info!("addr: {}", addr);
    let url = format!("ws://{}", addr);

    let n_clients = 5;
    let clients = (0..n_clients).map(|_x| mk_client(&url));
    let clients: Vec<Arc<Client>> = try_join_all(clients).await.unwrap();


    let tx_size = 256;
    for c in clients {
        tokio::task::spawn(
            loop_(tx_size, c.clone())
        );
    }

    Ok(())
}*/


#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let addr = std::env::args().nth(1).expect("no addr given");

    tracing::info!("addr: {}", addr);
    let url = format!("ws://{}", addr);

    let client = WsClientBuilder::default().build(url).await?;


    let tx_size = 256;
    loop {
        let random_bytes: Vec<u8> = (0..tx_size).map(|_| { random::<u8>() }).collect();
        let start = SystemTime::now();
        let _response: String = client.request("submit_transaction", rpc_params![random_bytes]).await?;
        let end = SystemTime::now();
        let duration = end.duration_since(start).unwrap();
        println!("tx included in preblock in {} ms", duration.as_millis());
        //tracing::info!("response: {}", response);
    }

    Ok(())
}
