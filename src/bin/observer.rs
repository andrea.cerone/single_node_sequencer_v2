use std::error::Error as StdError;
use std::net::SocketAddr;
use std::sync::Arc;
use std::sync::atomic::{AtomicU32, AtomicUsize, Ordering};

use async_channel::{Receiver, Sender};
use futures::FutureExt;
use hyper::server::conn::AddrStream;
use jsonrpsee::core::async_trait;
use jsonrpsee::Methods;
use jsonrpsee::proc_macros::rpc;
use jsonrpsee::server::{ConnectionGuard, ConnectionState, RpcServiceBuilder, ServerConfig, stop_channel, StopHandle, TowerServiceBuilder, ws};
use jsonrpsee::server::http::response;
use jsonrpsee::types::ErrorObjectOwned;
use jsonrpsee::ws_client::WsClientBuilder;
use jsonrpsee_core::{ClientError, rpc_params};
use jsonrpsee_core::client::async_client::Client;
use jsonrpsee_core::client::ClientT;
use jsonrpsee_core::params::ArrayParams;
use serde::Serialize;
use tokio::join;
use tokio::sync::Mutex;
use tower::Service;

#[derive(Default, Clone)]
struct Metrics {
    ws_connections: Arc<AtomicUsize>,
}

type Transaction = Vec<u8>;

#[derive(Clone, Serialize)]
enum TransactionStatus { Failed, Passed }

type Transactions = Vec<Transaction>;

#[derive(Clone)]
struct PreBlocks {
    elements: Arc<Mutex<Vec<Transactions>>>,
    sender: Sender<Transactions>,
    receiver: Receiver<Transactions>,
}


impl PreBlocks {
    fn new() -> Self {
        let (sender, receiver) = async_channel::unbounded();
        PreBlocks { elements: Arc::new(Mutex::new(vec![])), sender, receiver }
    }

    async fn push(&mut self, txs: Vec<Transaction>) {
        self.elements.lock().await.push(txs.clone());
        let sender = self.sender.clone();
        // Send the item to the sender stream
        sender.send(txs.clone()).await.unwrap();
        tracing::info!("pushed preblock");
    }
}

#[rpc(server)]
pub trait Api {
    #[method(name = "submit_transaction")]
    async fn submit_transaction(&self, tx: Transaction) -> Result<TransactionStatus, ErrorObjectOwned>;

    #[method(name = "submit_preblock")]
    async fn submit_preblock(&self, txs: Transactions) -> Result<(), ErrorObjectOwned>;
}

struct RpcImpl {
    client: Client,
    preblocks: Arc<Mutex<PreBlocks>>,
    receiver: Receiver<Transactions>,
}

fn new(c: Client) -> RpcImpl {
    let preblocks = PreBlocks::new();
    let receiver = preblocks.receiver.clone();
    RpcImpl { client: c, preblocks: Arc::new(Mutex::new(preblocks)), receiver }
}

#[async_trait]
impl ApiServer for RpcImpl {
    async fn submit_transaction(&self, tx: Transaction) -> Result<TransactionStatus, ErrorObjectOwned> {


        // send tx to sequencer, returns ()
        /*let a = async {

            loop {
                let preblock = self.receiver.recv().await.unwrap();
                tracing::info!("preblock pushed!");
                if preblock.contains(&tx) {
                    tracing::info!("tx sequenced!");
                    break
                }
            };
            Ok::<(), ClientError>(())
        };*/
        //let _ = self.client.request::<(), ArrayParams>("sequence_transaction", rpc_params![tx.clone()]).await.unwrap();
        // wait for preblock
        /*tracing::info!("initiate tx search!");
        let pb = self.preblocks.lock().await.elements.lock().await.clone();
        */

        let a = async {
            let mut found = false;
            loop {
                let pb = self.preblocks.lock().await.elements.lock().await.clone();
                for tx_ in pb.iter().rev().take(2).flatten() {
                    if *tx_ == tx {
                        tracing::info!("tx sequenced!");
                        found = true;
                        break;
                    }
                }
                if found { break Ok::<(), ClientError>(()); }
            }
        };

        //tracing::info!("tx search done! found={}", found);
        let _ = join!(a, self.client.request::<(), ArrayParams>("sequence_transaction", rpc_params![tx.clone()]));
        Ok(TransactionStatus::Passed)
        /*if found {
            Ok(TransactionStatus::Passed)
        } else {
            let result = loop {
                let preblock = self.receiver.recv().await.unwrap();
                tracing::info!("preblock pushed!");
                if preblock.contains(&tx) {
                    tracing::info!("tx sequenced!");
                    break Ok(TransactionStatus::Passed);
                }
            };

            // wait for key shares
            result
        }*/
    }

    async fn submit_preblock(&self, txs: Transactions) -> Result<(), ErrorObjectOwned> {
        tracing::info!("Received preblock of length: {}", txs.len());
        self.preblocks.lock().await.push(txs).await;
        Ok(())
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let addr = 9940;
    let () = run_server(addr).await;
    tracing::info!("addr: {}", addr);

    Ok(())
}


async fn run_server(addr: u16) -> () {
    use hyper::service::{make_service_fn, service_fn};

    let addr = SocketAddr::from(([127, 0, 0, 1], addr));

    // This state is cloned for every connection
    // all these types based on Arcs and it should
    // be relatively cheap to clone them.
    //
    // Make sure that nothing expensive is cloned here
    // when doing this or use an `Arc`.
    #[derive(Clone)]
    struct PerConnection<RpcMiddleware, HttpMiddleware> {
        methods: Methods,
        stop_handle: StopHandle,
        metrics: Metrics,
        conn_id: Arc<AtomicU32>,
        conn_guard: ConnectionGuard,
        svc_builder: TowerServiceBuilder<RpcMiddleware, HttpMiddleware>,
    }

    // Each RPC call/connection get its own `stop_handle`
    // to able to determine whether the server has been stopped or not.
    //
    // To keep the server running the `server_handle`
    // must be kept and it can also be used to stop the server.
    let (stop_handle, _server_handle) = stop_channel();


    let url = format!("ws://{}", "127.0.0.1:9949");

    let client = WsClientBuilder::default().build(&url).await.unwrap();

    let rpc = new(client);

    let per_conn = PerConnection {
        methods: rpc.into_rpc().into(),
        stop_handle: stop_handle.clone(),
        metrics: Metrics::default(),
        conn_id: Default::default(),
        conn_guard: ConnectionGuard::new(100),
        svc_builder: jsonrpsee::server::Server::builder()
            .max_connections(100)
            .to_service_builder(),
    };

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(move |_conn: &AddrStream| {
        let per_conn = per_conn.clone();

        async move {
            Ok::<_, Box<dyn StdError + Send + Sync>>(service_fn(move |req| {
                let is_websocket = ws::is_upgrade_request(&req);
                let PerConnection { methods, stop_handle, conn_id, conn_guard, metrics, svc_builder } = per_conn.clone();

                /*let mut svc = svc_builder.build(methods, stop_handle);
                async move {
                    let rp = svc.call(req).await;
                    if is_websocket {
                        metrics.ws_connections.fetch_add(1, Ordering::Relaxed);
                    }
                    rp
                }*/

                // jsonrpsee expects a `conn permit` for each connection.
                //
                // This may be omitted if don't want to limit the number of connections
                // to the server.
                let Some(conn_permit) = conn_guard.try_acquire() else {
                    return async { Ok::<_, Box<dyn StdError + Send + Sync>>(response::too_many_requests()) }.boxed();
                };

                if is_websocket {
                    let rpc_service = RpcServiceBuilder::new();


                    let conn = ConnectionState::new(stop_handle.clone(), conn_id.fetch_add(1, Ordering::Relaxed), conn_permit);

                    // Establishes the websocket connection
                    async move {
                        match ws::connect(req, ServerConfig::default(), methods.clone(), conn, rpc_service).await {
                            Ok((rp, conn_fut)) => {
                                tokio::spawn(async move {
                                    // Keep the connection alive until
                                    // a close signal is sent.
                                    // TODO
                                    conn_fut.await
                                });
                                Ok(rp)
                            }
                            Err(rp) => {
                                Ok(rp)
                            }
                        }
                    }
                        .boxed()
                } else if !ws::is_upgrade_request(&req) {
                    let rpc_service = RpcServiceBuilder::new();

                    let server_cfg = ServerConfig::default();
                    let conn = ConnectionState::new(stop_handle, conn_id.fetch_add(1, Ordering::Relaxed), conn_permit);

                    // There is another API for making call with just a service as well.
                    //
                    // See [`jsonrpsee::server::http::call_with_service`]
                    async move {
                        // Rpc call finished successfully.
                        Ok(jsonrpsee::server::http::call_with_service_builder(req, server_cfg, conn, methods, rpc_service).await)
                    }
                        .boxed()
                } else {
                    async { Ok(http::response::Response::default()) }.boxed()
                }
            }))
        }
    });

    if let Err(err) = hyper::Server::bind(&addr).serve(make_service).await {
        eprintln!("server error: {}", err);
    }
}