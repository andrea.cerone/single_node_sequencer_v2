use std::error::Error as StdError;
use std::net::SocketAddr;
use std::sync::Arc;
use std::sync::atomic::{AtomicU32, AtomicUsize, Ordering};
use std::time::Duration;

use async_channel::{Receiver, Sender};
use futures::FutureExt;
use hyper::server::conn::AddrStream;
use jsonrpsee::core::async_trait;
use jsonrpsee::Methods;
use jsonrpsee::proc_macros::rpc;
use jsonrpsee::server::{ConnectionGuard, ConnectionState, RpcServiceBuilder, ServerConfig, stop_channel, StopHandle, TowerServiceBuilder};
use jsonrpsee::server::http::response;
use jsonrpsee::server::ws;
use jsonrpsee::types::ErrorObjectOwned;
use jsonrpsee::ws_client::WsClientBuilder;
use jsonrpsee_core::client::{Client, ClientT};
use jsonrpsee_core::rpc_params;
use tokio::join;
use tokio::sync::Mutex;

const time_between_non_empty_blocks: Duration = Duration::new(0, 500_000_000);


type Transaction = Vec<u8>;
type Transactions = Vec<Transaction>;

#[derive(Clone)]
struct PreBlocks {
    elements: Arc<Mutex<Vec<Transactions>>>,
    sender: Sender<Transactions>,
    receiver: Receiver<Transactions>,
}


impl PreBlocks {
    fn new() -> Self {
        let (sender, receiver) = async_channel::unbounded();
        PreBlocks { elements: Arc::new(Mutex::new(vec![])), sender, receiver }
    }

    async fn push(&mut self, txs: Vec<Transaction>) {
        self.elements.lock().await.push(txs.clone());
        let sender = self.sender.clone();
        // Send the item to the sender stream
        sender.send(txs.clone()).await.unwrap();
        tracing::info!("pushed preblock");
    }
}

#[derive(Default, Clone)]
struct Metrics {
    ws_connections: Arc<AtomicUsize>,
}

type TransactionsPool = Arc<Mutex<Vec<Transaction>>>;

#[rpc(server)]
pub trait Api {
    #[method(name = "sequence_transaction")]
    async fn sequence_transaction(&self, tx: Transaction) -> Result<(), ErrorObjectOwned>;
}


struct RpcImpl {
    transactions_pool: TransactionsPool,
}

fn new(transactions_pool: TransactionsPool) -> RpcImpl {
    RpcImpl { transactions_pool: transactions_pool }
}

#[async_trait]
impl ApiServer for RpcImpl {
    async fn sequence_transaction(&self, tx: Transaction) -> Result<(), ErrorObjectOwned> {
        self.transactions_pool.lock().await.push(tx);
        Ok(())
    }
}

async fn push_preblocks(preblocks: &mut PreBlocks, time_between_empty_blocks: i32, transactions_pool: Arc<Mutex<Vec<Transaction>>>, max_txs_in_block: usize) {
    let mut count = 1;
    loop {
        tokio::time::sleep(time_between_non_empty_blocks).await;
        let mut tx_pool = transactions_pool.lock().await;
        if count == time_between_empty_blocks && tx_pool.is_empty() {
            preblocks.push(Vec::new()).await;
            let len = tx_pool.len();
            tracing::info!("Created empty preblock {}", len);
            count = 0;
        } else if tx_pool.is_empty() {
            count += 1;
        } else {
            let len = tx_pool.len();
            let final_length = if len < max_txs_in_block { len } else { max_txs_in_block };
            preblocks.push(tx_pool.drain(0..final_length).collect()).await;
            tracing::info!("Created preblock with {} transactions", final_length);
        }
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let addr = 9949;

    let max_txs_in_block = 25;
    let time_between_empty_blocks = 12; // as a factor of time_between_non_empty_blocks

    let transactions_pool = Arc::new(Mutex::new(Vec::new()));

    let mut preblocks = PreBlocks::new();
    let receiver = preblocks.receiver.clone();


    //let client = Arc::new(WsClientBuilder::default().build("ws://127.0.0.1:9940").await.unwrap());


    let a = run_server(addr, transactions_pool.clone(), receiver);
    let b = tokio::task::spawn(async move { push_preblocks(&mut preblocks, time_between_empty_blocks, transactions_pool, max_txs_in_block).await });
    join!(a, b);

    tracing::info!("Server listening at: {}", addr);

    Ok(())
}

async fn run_server(addr: u16, tx_pool: TransactionsPool, receiver: Receiver<Transactions>) -> () {
    use hyper::service::{make_service_fn, service_fn};

    let addr = SocketAddr::from(([127, 0, 0, 1], addr));

    // This state is cloned for every connection
    // all these types based on Arcs and it should
    // be relatively cheap to clone them.
    //
    // Make sure that nothing expensive is cloned here
    // when doing this or use an `Arc`.
    #[derive(Clone)]
    struct PerConnection<RpcMiddleware, HttpMiddleware> {
        methods: Methods,
        stop_handle: StopHandle,
        conn_id: Arc<AtomicU32>,
        conn_guard: ConnectionGuard,
        metrics: Metrics,
        svc_builder: TowerServiceBuilder<RpcMiddleware, HttpMiddleware>,
    }

    // Each RPC call/connection get its own `stop_handle`
    // to able to determine whether the server has been stopped or not.
    //
    // To keep the server running the `server_handle`
    // must be kept and it can also be used to stop the server.
    let (stop_handle, _server_handle) = stop_channel();

    let rpc = new(tx_pool);

    // TODO: per_conn should be an arc
    let per_conn = PerConnection {
        methods: rpc.into_rpc().into(),
        stop_handle: stop_handle.clone(),
        metrics: Metrics::default(),
        conn_id: Default::default(),
        conn_guard: ConnectionGuard::new(100),
        svc_builder: jsonrpsee::server::Server::builder()
            .max_connections(100)
            .to_service_builder(),
    };

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(move |_conn: &AddrStream| {
        let per_conn = per_conn.clone();
        let receiver = receiver.clone();
        let _remote_addr = _conn.remote_addr();
        tracing::info!("remote addr: {}", _remote_addr);
        //let client = client.clone();
        async move {
            Ok::<_, Box<dyn StdError + Send + Sync>>(service_fn(move |req| {
                let is_websocket = ws::is_upgrade_request(&req);
                let PerConnection { methods, stop_handle, conn_id, conn_guard, metrics, svc_builder } = per_conn.clone();

                let _svc = svc_builder.build(methods.clone(), stop_handle.clone());
                //let receiver = receiver.clone();

                // jsonrpsee expects a `conn permit` for each connection.
                //
                // This may be omitted if don't want to limit the number of connections
                // to the server.
                let Some(conn_permit) = conn_guard.try_acquire() else {
                    return async { Ok::<_, Box<dyn StdError + Send + Sync>>(response::too_many_requests()) }.boxed();
                };


                // You can't determine whether the websocket upgrade handshake failed or not here.

                //let rp = svc.call(req).await;
                if is_websocket {
                    metrics.ws_connections.fetch_add(1, Ordering::Relaxed);


                    let rpc_service = RpcServiceBuilder::new();

                    let conn = ConnectionState::new(stop_handle.clone(), conn_id.fetch_add(1, Ordering::Relaxed), conn_permit);
                    let receiver = receiver.clone();
                    //let client = client.clone();
                    // Establishes the websocket connection
                    // the connection is closed i.e, when the `conn_fut` is dropped.
                    async move {

                        match ws::connect(req, ServerConfig::default(), methods.clone(), conn, rpc_service).await {
                            Ok((rp, conn_fut)) => {
                                //let receiver = receiver.clone();

                                tokio::task::spawn(async move {
                                    // Keep the connection alive until
                                    // a close signal is sent.
                                    conn_fut.await;

                                });
                                tokio::task::spawn(async move {

                                    tokio::time::sleep(Duration::new(1, 0)).await;
                                    let client = WsClientBuilder::default().build("ws://127.0.0.1:9940").await.unwrap();
                                    while let Ok(preblock) = receiver.recv().await {
                                        tracing::info!("New preblock added to queue: {:?}", preblock);
                                        let _response: () = client.request("submit_preblock", rpc_params![preblock]).await.unwrap();
                                    }
                                });
                                Ok(rp)
                            }
                            Err(rp) => {
                                Ok(rp)
                            }
                        }
                    }
                        .boxed()
                } else if !ws::is_upgrade_request(&req) {
                    let rpc_service = RpcServiceBuilder::new();

                    let server_cfg = ServerConfig::default();
                    let conn = ConnectionState::new(stop_handle, conn_id.fetch_add(1, Ordering::Relaxed), conn_permit);

                    // There is another API for making call with just a service as well.
                    //
                    // See [`jsonrpsee::server::http::call_with_service`]
                    async move {
                        // Rpc call finished successfully.
                        Ok(jsonrpsee::server::http::call_with_service_builder(req, server_cfg, conn, methods, rpc_service).await)
                    }
                        .boxed()
                } else {
                    async { Ok(http::response::Response::default()) }.boxed()
                }
            }))
        }
    });

    let server = hyper::Server::bind(&addr).serve(make_service);


    let _ = server.await;
}