use jsonrpsee::core::client::ClientT;
use jsonrpsee::rpc_params;
use jsonrpsee::ws_client::WsClientBuilder;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .init();

    let addr = std::env::args().nth(1).expect("no addr given");

    tracing::info!("addr: {}", addr);
    let url = format!("ws://{}", addr);

    let client = WsClientBuilder::default().build(&url).await?;

    let response: String = client.request("submit_transaction", rpc_params![vec![1u8]]).await?;
    tracing::info!("response: {}", response);


    Ok(())
}
