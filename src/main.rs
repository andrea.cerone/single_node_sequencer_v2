use std::net::SocketAddr;
use jsonrpsee::core::client::ClientT;
use jsonrpsee::server::{RpcServiceBuilder, Server};
use jsonrpsee::ws_client::WsClientBuilder;
use jsonrpsee::rpc_params;
use jsonrpsee::types::ErrorObjectOwned;
use jsonrpsee::core::async_trait;
use jsonrpsee::proc_macros::rpc;

#[rpc(server)]
pub trait Api {
    #[method(name = "multiply")]
    fn multiply(&self, n: usize) -> Result<usize, ErrorObjectOwned>;

    #[method(name = "multiply_async")]
    async fn multiply_async(&self, n: usize) -> Result<usize, ErrorObjectOwned>;
}

struct RpcImpl {
    state: usize,
}

fn new(n: usize) -> RpcImpl {
    RpcImpl { state: n }
}

#[async_trait]
impl ApiServer for RpcImpl {
    fn multiply(&self, n: usize) -> Result<usize, ErrorObjectOwned> {
        Ok(n * self.state)
    }

    async fn multiply_async(&self, n: usize) -> Result<usize, ErrorObjectOwned> {
        Ok(n * self.state)
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .with_target(false)
        .init();

    let addr = run_server().await?;
    let url = format!("ws://{}", addr);

    let client = WsClientBuilder::default().build(&url).await?;
    let response: usize = client.request("multiply", rpc_params![12usize]).await?;
    tracing::info!("response: {}", response);

    Ok(())
}

async fn run_server() -> anyhow::Result<SocketAddr> {
    let rpc_middleware = RpcServiceBuilder::new().rpc_logger(1024);
    let server = Server::builder().set_rpc_middleware(rpc_middleware).build("127.0.0.1:0").await?;

    let addr = server.local_addr()?;
    let handle = server.start(new(2).into_rpc());

    // In this example we don't care about doing shutdown so let it run forever.
    // You may use the `ServerHandle` to shut it down or manage it yourself.
    tokio::spawn(handle.stopped());

    Ok(addr)
}